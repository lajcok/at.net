﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HasherLib1
{
    public class LengthHasher
    {
        public string Hash(string toHash)
        {
            return toHash.Length.ToString();
        }
    }
}
