﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using Web_App.Models;

namespace Web_App
{
    public class ProductRepository
    {
        /* Properties */

        private static readonly string dbPath = Path.GetFullPath("Products.xml");

        private Dictionary<int, Product> _products;
        public Dictionary<int, Product> Products { get => _products ?? (_products = Load()); }

        public void Save(Product product)
        {
            if (product.ID == null)
            {
                int max = 0;
                foreach (int id in Products.Keys)
                {
                    if (id > max)
                    {
                        max = id;
                    }
                }

                int nid = max + 1;
                product.ID = nid;
                Products.Add(nid, product);
            }
            else
            {
                Products[(int)product.ID] = product;
            }

            Save();
        }

        public void Delete(int id)
        {
            Products.Remove(id);
            Save();
        }

        /* Storage */

        private Dictionary<int, Product> Load()
        {
            if(!File.Exists(dbPath))
            {
                return new Dictionary<int, Product>(0);
            }

            XmlSerializer xml = new XmlSerializer(typeof(List<Product>));
            List<Product> products;
            using(FileStream file = File.OpenRead(dbPath))
            {
                products = xml.Deserialize(file) as List<Product>;
            }

            var res = new Dictionary<int, Product>(products.Count);
            foreach(var product in products)
            {
                res.Add((int)product.ID, product);
            }
            return res;
        }

        private void Save()
        {
            XmlSerializer xml = new XmlSerializer(typeof(List<Product>));
            using (FileStream file = File.Open(dbPath, FileMode.Create))
            {
                xml.Serialize(file, Products.Values.ToList());
            }
        }

    }
}
