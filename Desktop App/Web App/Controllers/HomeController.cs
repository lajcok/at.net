﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Web_App.Models;

namespace Web_App.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index(ProductRepository repository)
        {
            return View(repository.Products.Values.ToArray());
        }

        public IActionResult Detail(int id, ProductRepository repository)
        {
            try
            {
                return View(repository.Products[id]);
            }
            catch
            {
                return RedirectToAction("Index");
            }

        }

        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Login(Login loginForm)
        {
            var check = new Login()
            {
                Email = "admin@example.com",
                Password = "admin",
            };

            if (ModelState.IsValid)
            {
                if (check.Email != loginForm.Email || check.Password != loginForm.Password)
                {
                    ModelState.AddModelError("Email", "Wrong credentials!");
                }
            }

            if (ModelState.IsValid)
            {
                HttpContext.Session.SetString("User", loginForm.Email);
                return RedirectToAction("Index", "Product");
            }

            return View();
        }

        public IActionResult Logout()
        {
            HttpContext.Session.Remove("User");
            return RedirectToAction("Login");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
