﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web_App.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Web_App.Controllers
{
    public class ProductController : Controller
    {

        /* Properties */

        private readonly ProductRepository repository;

        /* Initialization */

        public ProductController(ProductRepository repository)
        {
            this.repository = repository;
        }

        /* Controller */

        public override void OnActionExecuted(ActionExecutedContext context)
        {
            string email = HttpContext.Session.GetString("User");
            if (string.IsNullOrEmpty(email))
            {
                context.Result = RedirectToAction("Index", "Home");
                return;
            }

            base.OnActionExecuted(context);
        }
        
        public IActionResult Index()
        {
            return View(repository.Products.Values.ToArray());
        }
        
        public IActionResult Edit(int? id)
        {
            return View(id != null ? repository.Products[(int)id] : null);
        }

        [HttpPost]
        public IActionResult Edit(Product product, int? id)
        {
            if(ModelState.IsValid)
            {
                if(id != null)
                {
                    product.ID = id;
                }
                repository.Save(product);
                return RedirectToAction("Index");
            }
            return View(product);
        }
        
        public IActionResult Delete(int id)
        {
            repository.Delete(id);
            return RedirectToAction("Index");
        }
    }
}