﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Desktop_App
{
    public interface IHasher
    {
        string Hash(string toHash);
    }
}
