﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Desktop_App
{
    public partial class Main : Form
    {
        private readonly HistoryXml history;
        public Main()
        {
            InitializeComponent();

            var plugins = new Plugins();
            foreach(var hasher in plugins.Hashers)
            {
                comboBox1.Items.Add(hasher);
            }
            comboBox1.SelectedItem = plugins.Hashers.First();

            history = new HistoryXml(treeView1);
            history.Load();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var input = textBox1.Text;
            var plugin = comboBox1.SelectedItem as Plugin;
            var hash = plugin.Hash(input);

            textBox2.Text = hash;

            TreeNode pluginNode;
            if (!treeView1.Nodes.ContainsKey(plugin.ToString()))
            {
                pluginNode = treeView1.Nodes.Add(plugin.ToString(), plugin.ToString());
            }
            else
            {
                pluginNode = treeView1.Nodes[plugin.ToString()];
            }

            if(!pluginNode.Nodes.ContainsKey(input))
            {
                pluginNode.Nodes.Add(input, input + ": " + hash);
                history.Save();
            }
        }
    }
}
