﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Desktop_App
{
    public class Plugins
    {
    

        /* Properties */

        private List<Plugin> _hashers;
        public List<Plugin> Hashers
        {
            get
            {
                if(_hashers == null)
                {
                    LoadAll();
                }
                return _hashers;
            }
        }

        public void LoadAll()
        {
            var current = Path.GetFullPath(@".\");
            string[] dlls = Directory.GetFiles(current, "*.dll");

            Debug.WriteLine("Loaded " + dlls.Length + " dlls");
            Debug.Indent();

            _hashers = new List<Plugin>(dlls.Length);
            foreach(var dll in dlls)
            {
                Assembly assembly = Assembly.LoadFile(Path.GetFullPath(dll));
                Debug.WriteLine("Assembly scanned: " + assembly.FullName, "Assembly");
                Debug.Indent();

                var types = assembly.GetTypes();
                int i = 0;
                foreach (var type in types)
                {
                    if(type.IsInterface)
                    {
                        Debug.WriteLine("Interface " + type.FullName + " skipped", "Type");
                        continue;
                    }

                    var method = type.GetMethod("Hash");
                    if(method == null)
                    {
                        Debug.WriteLine("Type " + type.FullName + " skipped, no Hash method", "Type");
                        continue;
                    }

                    var pars = method.GetParameters();
                    Trace.Assert(
                        pars.Length == 1 && pars.First().ParameterType == typeof(string),
                        "The Hash() method has invalid signature!"
                    );

                    _hashers.Add(new Plugin(type, assembly));
                    i++;
                }

                Debug.WriteLine("Loaded " + i + " out of " + types.Length + " types.");
                Debug.Assert(i > 0, "No usable hashers found in assembly " + assembly.FullName + "!");
                Debug.Unindent();
            }

            Trace.Assert(_hashers.Count > 0, "No hashers added at all!");
            Debug.Unindent();
        }

    }
}
