﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Desktop_App
{
    public class Plugin: IHasher
    {
        private Type _hasher;
        private Assembly _assembly;

        public Plugin(Type hasher, Assembly assembly)
        {
            _hasher = hasher;
            _assembly = assembly;
        }

        public Type Hasher { get => _hasher; }

        public override string ToString()
        {
            return _hasher.FullName;
        }

        public string Hash(string toHash)
        {
            var instance = _assembly.CreateInstance(_hasher.FullName);
            return Hasher.GetMethod("Hash")
                ?.Invoke(instance, new object[] { toHash })
                .ToString();
        }
    }
}
