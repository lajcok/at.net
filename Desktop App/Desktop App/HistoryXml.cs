﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Desktop_App
{
    class HistoryXml
    {

        private static readonly string outfile = Path.GetFullPath("history.xml");
        private readonly TreeView treeView;

        public HistoryXml(TreeView treeView)
        {
            this.treeView = treeView;
        }

        public void Save()
        {
            XmlDocument xml = new XmlDocument();

            XmlDeclaration dec = xml.CreateXmlDeclaration("1.0", "utf-8", null);
            xml.AppendChild(dec);

            XmlElement root = xml.CreateElement("History");
            xml.AppendChild(root);

            foreach (TreeNode pluginNode in treeView.Nodes)
            {
                XmlElement pluginEl = xml.CreateElement("Plugin");
                root.AppendChild(pluginEl);

                XmlAttribute nameAttr = xml.CreateAttribute("Name");
                nameAttr.InnerText = pluginNode.Text;
                pluginEl.Attributes.Append(nameAttr);

                foreach (TreeNode hashNode in pluginNode.Nodes)
                {
                    XmlElement hashEl = xml.CreateElement("Hash");
                    pluginEl.AppendChild(hashEl);

                    XmlAttribute inputAttr = xml.CreateAttribute("Input");
                    inputAttr.InnerText = hashNode.Name;
                    hashEl.Attributes.Append(inputAttr);

                    // Extract from format "input: hash"
                    hashEl.InnerText = hashNode.Text.Substring(hashNode.Name.Length + 2);
                }
            }

            xml.Save(outfile);
        }

        public void Load()
        {
            if(!File.Exists(outfile))
            {
                return;
            }

            XmlDocument xml = new XmlDocument();
            xml.Load(outfile);
            treeView.Nodes.Clear();

            foreach (XmlElement plugin in xml.SelectNodes("/History/Plugin"))
            {
                string pluginName = plugin.GetAttribute("Name");
                TreeNode pluginNode = treeView.Nodes.Add(pluginName, pluginName);

                foreach (XmlElement hashEl in plugin.SelectNodes("./Hash"))
                {
                    string input = hashEl.GetAttribute("Input");
                    string hash = hashEl.InnerText;
                    pluginNode.Nodes.Add(input, input + ": " + hash);
                }
            }
        }

    }
}
