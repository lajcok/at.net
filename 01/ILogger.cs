﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cv04_01
{
    public interface ILogger
    {

        void Log(string message);

    }
}
