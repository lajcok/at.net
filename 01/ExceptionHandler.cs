﻿using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Cv04_01
{
    public class ExceptionHandler
    {

        private ILogger log;

        public ExceptionHandler(ILogger log)
        {
            this.log = log;
        }

        public void Handle(Exception err)
        {
            this.log.Log(err.Message);
        }

    }
}
