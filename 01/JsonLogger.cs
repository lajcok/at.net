﻿using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Cv04_01
{
    public class JsonLogger : ILogger
    {

        private IHostingEnvironment env;

        public JsonLogger(IHostingEnvironment env)
        {
            this.env = env;
        }

        public void Log(string message)
        {
            File.AppendAllText(
                Path.Combine(this.env.ContentRootPath, "error.json"),
                JsonConvert.SerializeObject(message)
                );
        }

    }
}
