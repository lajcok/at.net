﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Cv04_01
{
    public class ErrorLogMiddleware
    {

        private readonly RequestDelegate next;

        public ErrorLogMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext context, IHostingEnvironment env, ExceptionHandler handler)
        {
            try
            {
                await next(context);
            }
            catch (Exception err)
            {
                handler.Handle(err);
            }
        }

    }
}
