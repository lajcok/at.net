﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cv04_01;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace _01
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<ExceptionHandler>();
            services.AddScoped<ILogger, JsonLogger>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMiddleware<ErrorLogMiddleware>();
            app.UseMiddleware<StaticContentMiddleware>();

            app.Run(async (context) =>
            {

                throw new Exception("Unexpected exception thrown!");
                var html = @"
<!DOCTYPE html>
<html>
<head>
<title>My Title</title>
</head>
<body>
<h1>Nadpisek</h1>
</body>
</html>
";
                context.Response.ContentType = "text/html";
                await context.Response.WriteAsync(html);
            });
        }
    }
}
