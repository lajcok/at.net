﻿using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Cv04_01
{
    public class TxtLogger : ILogger
    {

        private IHostingEnvironment env;

        public TxtLogger(IHostingEnvironment env)
        {
            this.env = env;
        }

        public void Log(string message)
        {
            File.AppendAllText(
                Path.Combine(this.env.ContentRootPath, "error.log"),
                message + '\n'
                );
        }

    }
}
