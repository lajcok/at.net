﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Cv04_01
{
    public class StaticContentMiddleware
    {

        private readonly RequestDelegate next;

        public StaticContentMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext context, IHostingEnvironment env)
        {
            string uri = context.Request.Path.Value;
            string root = env.WebRootPath;
            string path = Path.Combine(root, uri.TrimStart('/'));

            if (File.Exists(path))
            {
                await context.Response.SendFileAsync(path);
            }
            else
            {
                await next(context);
            }
        }

    }
}
