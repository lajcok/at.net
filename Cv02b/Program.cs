﻿using System;
using System.IO;

namespace Cv02b
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine(AppDomain.CurrentDomain.FriendlyName);
            Console.WriteLine(AppDomain.CurrentDomain.BaseDirectory);

            foreach (var arg in args)
            {
                File.AppendAllText(AppDomain.CurrentDomain.BaseDirectory + "args.txt", arg?.ToUpper() + '\n');
            }
        }
    }
}