﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Cv06.Models
{
    public class AddTaskForm
    {

        [Display(Name="Task")]
        [Required]
        public string Title { get; set; }

    }
}
