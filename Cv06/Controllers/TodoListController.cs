﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cv06.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Cv5_01.Controllers
{
    public class TodoListController : Controller
    {

        public override void OnActionExecuted(ActionExecutedContext context)
        {
            string email = HttpContext.Session.GetString("user");
            if (string.IsNullOrEmpty(email))
            {
                context.Result = RedirectToAction("Index", "Home");
                return;
            }

            base.OnActionExecuted(context);
        }

        public IActionResult Index()
        {
            ViewBag.Tasks = System.IO.File.Exists("tasks.txt") ?
                System.IO.File.ReadAllText("tasks.txt") : null;

            return View();
        }

        [HttpPost]
        public IActionResult Index(AddTaskForm addForm)
        {
            if(ModelState.IsValid)
            {
                System.IO.File.AppendAllText("tasks.txt", string.Format("[]{0}\n", addForm.Title));
                return RedirectToAction("Index");
            }
            return View();
        }

        public IActionResult Logout()
        {
            HttpContext.Session.Remove("User");
            return RedirectToAction("Index", "Home");
        }
    }
}