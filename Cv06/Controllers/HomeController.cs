﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Cv06.Models;
using Microsoft.AspNetCore.Http;

namespace Cv06.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Index(LoginForm loginForm)
        {
            var check = new LoginForm()
            {
                Email = "choc@example.com",
                Password = "123456",
            };

            if(ModelState.IsValid)
            {
                if(check.Email != loginForm.Email || check.Password != loginForm.Password)
                {
                    ModelState.AddModelError("Email", "Wrong credentials!");
                }
            }

            if(ModelState.IsValid)
            {
                HttpContext.Session.SetString("user", loginForm.Email);
                return RedirectToAction("Index", "TodoList");
            }

            return View();
        }
        
    }
}
