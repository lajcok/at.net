﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Cv5_01
{
    public class AddToCartForm
    {

        public int ProductId { get; set; }

        [Display(Name="Počet kusů")]
        public int Amount { get; set; }

    }
}
