﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Cv5_01.Models;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace Cv5_01.Controllers
{
    public class HomeController : Controller
    {
        private ProductService productService;

        public HomeController(ProductService productService)
        {
            this.productService = productService;
        }

        public IActionResult Index()
        {
            ViewBag.products = productService.GetProducts();
            return View();
        }

        public IActionResult Detail(int ID)
        {
            ViewBag.product = productService.GetProduct(ID);

            return View(new AddToCartForm()
            {
                ProductId = ID,
                Amount = 1
            });
        }

        [HttpPost]
        public IActionResult Detail(AddToCartForm form)
        {
            ViewBag.product = this.productService.GetProduct(form.ProductId);

            if (form.Amount <= 0)
            {
                ModelState.AddModelError("Amount", "Počet kusů musí být alespoň 1");
            }

            if (ModelState.IsValid)
            {
                var oldData = HttpContext.Session.GetString("cart");
                List<AddToCartForm> cartData;
                if (string.IsNullOrEmpty(oldData))
                {
                    cartData = new List<AddToCartForm>();
                }
                else
                {
                    cartData = JsonConvert.DeserializeObject<List<AddToCartForm>>(oldData);
                }
                cartData.Add(form);

                HttpContext.Session.SetString("cart", JsonConvert.SerializeObject(cartData));
                return RedirectToAction("Cart");
            }

            return View(form);
        }

        public IActionResult Cart()
        {
            var sessionData = HttpContext.Session.GetString("cart");
            ViewBag.cartData = JsonConvert.DeserializeObject<List<AddToCartForm>>(sessionData); 
            return View();
        }

    }

}
