﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cv5_01.Models;

namespace Cv5_01
{
    public class ProductService 
    {

        /* Properties */

        private readonly Product[] products = {
            new Product()
            {
                ID = 1,
                Title = "Skate komplet",
                Price = 3000
            },
            new Product()
            {
                ID = 2,
                Title = "Skate deska",
                Price = 999
            },
            new Product()
            {
                ID = 3,
                Title = "Skate kolečka",
                Price = 699
            },
        };

        /* Interface */

        public Product[] GetProducts()
        {
            return products;
        }

        public Product GetProduct(int ID)
        {
            foreach (var product in products)
            {
                if (product.ID == ID)
                {
                    return product;
                }
            }
            return null;
        }

    }
}
