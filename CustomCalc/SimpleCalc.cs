﻿using System;
using System.Security.Cryptography.X509Certificates;

namespace CustomCalc
{
    public class SimpleCalc
    {

        /* Properties */
        
        private int x;
        private int y;
        private int result;
        
        /* Private */

        private void Multiply()
        {
            result = x * y;
        }

        private void ShowResult()
        {
            Console.WriteLine("Result: {0}", result);
        }
        
        /* Interface */

        public void SetXY(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public void Add()
        {
            result = x + y;
        }

    }
}