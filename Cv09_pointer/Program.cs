﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cv09_pointer
{
    class Program
    {
        static void Main(string[] args)
        {
            var rand = new Random();
            var textB = new StringBuilder();
            for (int i = 0; i < 1000; i++)
            {
                textB.Append((char)(65 + rand.Next() % 40));
            }

            var stopwatch = new Stopwatch();
            stopwatch.Start();

            int cSum = 0;
            var text = textB.ToString();
            for (int i = 0; i < text.Length; i++)
            {
                for (int j = 1; j < text.Length - i; j++)
                {
                    cSum += CCount(text.Substring(i, j));
                }
            }
            /*for (int i = 0; i < text.Length; i++)
            {
                for (int j = i + 1; j < text.Length; j++)
                {
                    cSum += CCount(text.Substring(i, j - i));
                }
            }*/

            stopwatch.Stop();

            Console.WriteLine(cSum);
            Console.WriteLine("Čas: {0}", stopwatch.ElapsedMilliseconds);



            stopwatch.Reset();
            stopwatch.Start();
            cSum = 0;

            unsafe
            {
                fixed (char* textPointer = text)
                {
                    for (int i = 0; i < text.Length; i++)
                    {
                        for (int j = 1; j < text.Length - i; j++)
                        {
                            cSum += CCount(textPointer + i, j);
                        }
                    }
                }
            }

            stopwatch.Stop();

            Console.WriteLine(cSum);
            Console.WriteLine("Čas: {0}", stopwatch.ElapsedMilliseconds);
        }

        private static int CCount(string str)
        {
            int c = 0;
            foreach (var ch in str)
            {
                if (ch == 'c')
                {
                    c++;
                }
            }
            return c;
        }

        private static unsafe int CCount(char* str, int len)
        {
            int c = 0;
            for(int i = 0; i < len; i++)
            {
                if (str[i] == 'c')
                {
                    c++;
                }
            }
            return c;
        }
    }
}
