﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Xml;
using CurrencyGraphLib;

namespace CurrencyGraphTest
{
    class Program
    {

        static void Main(string[] args)
        {
            DoReport();
            return;

            /*
            var timer = new Timer(1 * 1 * 1000) { AutoReset = true };
            timer.Elapsed += (object sender, ElapsedEventArgs e) => { DoReport(); };
            timer.Start();

            Console.ReadLine();

            timer.Stop();
            timer.Dispose();
            */
        }
        
        private static void DoReport()
        {
            // Report download
            var downloader = new Downloader();
            var report = downloader.GetTodaysReport();

            // History log
            var history = new History();
            history.AddReport(report);

            // Graph
            var graph = new Graph(history);
            graph.Draw();

            // Email
            var email = new Email();
            email.Send("lajcok.jiri@gmail.com", graph.Output);
        }
    }
}
