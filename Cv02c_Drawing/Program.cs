﻿using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Net.Mime;

namespace Cv02c_Drawing
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            using (var bmp = new Bitmap(400, 200))
            {
                using (var g = Graphics.FromImage(bmp))
                {
                    /*
                    g.DrawLine(
                        new Pen(new SolidBrush(Color.Chartreuse), 10),
                        10, 10,
                        300, 300
                    );
                    
                    g.DrawString(
                        "Hello",
                        new Font(FontFamily.GenericMonospace, 30),
                        new SolidBrush(Color.Coral),
                        100, 10
                    );
                    
                    g.FillRectangle(
                        new HatchBrush(HatchStyle.Cross, Color.Aqua),
                        10, 100,
                        50, 50
                    );
                    */
                    
                    g.FillRectangle(
                        new SolidBrush(Color.White),
                        0, 0,
                        400, 200
                    );
                    
                    g.DrawRectangle(
                        new Pen(new SolidBrush(Color.Coral), 20),
                        10, 10,
                        380, 180
                    );
                    
                    g.DrawString(
                        "Jiří\n\nLajčoků",
                        new Font(FontFamily.GenericSerif, 20, FontStyle.Italic),
                        new SolidBrush(Color.Chocolate),
                        150, 40
                    );

                    using (var img = Image.FromFile("me.jpg"))
                    {
                        g.DrawImage(img, 30, 30, 100, 140);
                    }
                }
                
                bmp.Save("img.png");
                // bmp.Save("img.bmp", ImageFormat.Bmp);
            }
        }
    }
}