using System;
using System.Collections.Specialized;
using System.Reflection;
using System.Text;

namespace Cv03_1
{
    public class SimpleSerializer<T>
    {

        public static string Serialize(object obj)
        {
            var type = obj.GetType();
            var b = new StringBuilder();
            foreach (var property in type.GetProperties(
                // BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance
            ))
            {
                b.AppendLine($"{property.Name}:{property.GetValue(obj)}");
            }
            
            return b.ToString();
        }

        public static T Deserialize(string serialized)
        {
            var type = typeof(T);
            var instance = (T) Activator.CreateInstance(type);

            foreach (var ln in serialized.Split( "\r\n".ToCharArray() ))
            {
                var val = ln.Split(':');
                if(val == null || val.Length < 2)
                {
                    continue;
                }

                var name = val[0];
                var value = val[1];

                var prop = type.GetProperty(name);
                if (prop == null)
                {
                    continue;
                }
                
                if (prop.PropertyType == typeof(int))
                {
                    prop.SetValue(instance, int.Parse(value));
                }
                else
                {
                    prop.SetValue(instance, value);
                }
                
            }
            
            return instance;
        }
        
    }
}