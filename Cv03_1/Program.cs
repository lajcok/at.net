﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace Cv03_1
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var reflect = Assembly.LoadFile(Path.GetFullPath("../../../CustomCalc/bin/Debug/CustomCalc.dll"));
            var type = reflect.GetType("CustomCalc.SimpleCalc");
            
            foreach (var method in type.GetMethods(
                BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly
            )) {
                var vis = method.IsPublic
                    ? "public"
                    : (
                        method.IsPrivate ? "private" : "protected"
                    );
                
                var parameters = new ArrayList();
                foreach (var parameter in method.GetParameters())
                {
                    parameters.Add($"{parameter.GetType().Name} {parameter.Name}");
                }
                var attrs = string.Join(", ", parameters.ToArray());
                

                var fullName = $"{vis} {method.ReturnType.Name} {method.Name} ( {attrs} )";
                Console.WriteLine(fullName);
            }

            var instance = reflect.CreateInstance(type.FullName);
            
            var setXY = type.GetMethod("SetXY");
            setXY?.Invoke(instance, new object[] {1, 2});
            
            var multiply = type.GetMethod("Multiply", BindingFlags.Instance | BindingFlags.NonPublic);
            multiply?.Invoke(instance, new object[] {});
            
            var result = type.GetMethod("ShowResult", BindingFlags.Instance | BindingFlags.NonPublic);
            result?.Invoke(instance, new object[] {});




            var p = new Person
            {
                Name = "Jirik lajcok",
                Age = 25,
            };
            Console.WriteLine(p);
            
            var ser = SimpleSerializer<Person>.Serialize(p);
            Console.WriteLine(ser);

            var des = SimpleSerializer<Person>.Deserialize(ser);
            Console.WriteLine(des);
        }

        public class Person
        {
            public string Name { get; set; }
            public int Age { get; set; }
            public override string ToString()
            {
                return $"{Name} ({Age})";
            }
        }
    }
}