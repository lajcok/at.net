﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Cv07
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var client = new WebClient())
            {
                byte[] data = client.DownloadData("http://www.cnb.cz/en/financial_markets/foreign_exchange_market/exchange_rate_fixing/daily.txt");
                string content = Encoding.UTF8.GetString(data);

                Console.WriteLine(content);
                Console.WriteLine(ParseRate(content, "EUR"));

                var xml = ConvertToXml(content);
                xml.Save("currency.xml");
            }
        }

        static double? ParseRate(string content, string code)
        {
            foreach (string line in content.Split('\n'))
            {
                string[] cols = line.Split('|');

                if (cols.Length > 4 && cols[3].ToLower() == code.ToLower())
                {
                    return double.Parse(cols[4], CultureInfo.InvariantCulture);
                }
            }

            return null;
        }

        static XmlDocument ConvertToXml(string content)
        {
            XmlDocument xml = new XmlDocument();
            XmlNode header = xml.CreateXmlDeclaration("1.0", "UTF-8", null);
            xml.AppendChild(header);

            XmlElement currencies = xml.CreateElement("Currencies");
            xml.AppendChild(currencies);

            foreach (string line in content.Split('\n'))
            {
                string[] cols = line.Split('|');
                if(cols.Length <= 4 || cols[4] == "Rate")
                {
                    continue;
                }

                XmlElement currency = xml.CreateElement("Currency");
                currencies.AppendChild(currency);

                XmlAttribute code = xml.CreateAttribute("Code");
                code.Value = cols[3];
                currency.Attributes.Append(code);

                XmlElement name = xml.CreateElement("Name");
                name.InnerText = cols[1];
                currency.AppendChild(name);

                XmlElement country = xml.CreateElement("Country");
                country.InnerText = cols[0];
                currency.AppendChild(country);

                XmlElement amount = xml.CreateElement("Amount");
                amount.InnerText = cols[2];
                currency.AppendChild(amount);

                XmlElement rate = xml.CreateElement("Rate");
                rate.InnerText = cols[4];
                currency.AppendChild(rate);
            }

            return xml;
        }
    }
}
