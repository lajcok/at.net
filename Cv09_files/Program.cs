﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cv09_files
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = Path.GetFullPath(args[0]);
            Console.WriteLine(path);

            var fsw = new FileSystemWatcher(path);

            fsw.Created += OnFileAction;
            fsw.Deleted += OnFileAction;
            fsw.Changed += OnFileAction;
            fsw.Renamed += OnFileAction;

            fsw.IncludeSubdirectories = true;
            //fsw.Filter = "*.jpg";
            fsw.EnableRaisingEvents = true;
            
            Console.ReadLine();
        }

        private static void OnFileAction(object sender, FileSystemEventArgs e)
        {
            Console.WriteLine("{0}: {1}", e.ChangeType, e is RenamedEventArgs ? (((RenamedEventArgs)e).OldName + " > " + e.Name) : e.Name);
        }
    }
}
