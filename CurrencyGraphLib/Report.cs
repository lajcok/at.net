﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurrencyGraphLib
{
    public class Report
    {

        /* Properties */

        public DateTime Date { get; set; }

        public List<Currency> Currencies { get; } = new List<Currency>();
    }
}
