﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CurrencyGraphLib
{
    public class History
    {
        /* Initialization */

        private readonly string dbPath;

        public History(string dbPath = "History.xml")
        {
            this.dbPath = Path.GetFullPath(dbPath);
        }

        /* Properties */

        public List<Report> GetReports(int newest = -1)
        {
            if (!File.Exists(dbPath))
            {
                return null;
            }

            var xml = new XmlDocument();
            xml.Load(dbPath);
            var root = xml.DocumentElement;

            var reports = new List<Report>();
            string xpath = "./Report";
            if(newest > 0)
            {
                xpath += $"[position() > last() - {newest}]";
            }

            foreach (XmlElement reportEl in root.SelectNodes(xpath))
            {
                var report = new Report();

                var dayAttr = reportEl.GetAttribute("Day");
                report.Date = DateTime.ParseExact(dayAttr, "yyyy-MM-dd", CultureInfo.CurrentCulture);

                foreach (XmlElement item in reportEl.SelectNodes("./Currency"))
                {
                    var code = item.GetAttribute("Code");
                    var rate = item.GetAttribute("Rate");
                    var currency = new Currency()
                    {
                        Code = code,
                        Rate = Convert.ToDecimal(rate),
                    };
                        
                    report.Currencies.Add(currency);
                }

                reports.Add(report);
            }

            return reports;
        }

        public void AddReport(Report report)
        {

            var xml = new XmlDocument();
            if(!File.Exists(dbPath))
            {
                var header = xml.CreateXmlDeclaration("1.0", "UTF-8", null);
                xml.AppendChild(header);
            }
            else
            {
                xml.Load(dbPath);
            }

            var root = xml.DocumentElement;
            if(root == null)
            {
                root = xml.CreateElement("History");
                xml.AppendChild(root);
            }

            var reportEl = xml.CreateElement("Report");
            var dayAttr = xml.CreateAttribute("Day");
            dayAttr.Value = report.Date.ToString("yyyy-MM-dd");
            reportEl.Attributes.Append(dayAttr);

            foreach(var currency in report.Currencies)
            {
                var currencyEl = xml.CreateElement("Currency");

                var codeAttr = xml.CreateAttribute("Code");
                codeAttr.Value = currency.Code;
                currencyEl.Attributes.Append(codeAttr);

                var rateAttr = xml.CreateAttribute("Rate");
                rateAttr.Value = currency.Rate.ToString();
                currencyEl.Attributes.Append(rateAttr);

                reportEl.AppendChild(currencyEl);
            }

            root.AppendChild(reportEl);
            xml.Save(dbPath);
        }

    }
}
