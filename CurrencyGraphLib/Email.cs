﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace CurrencyGraphLib
{
    public class Email
    {
        public void Send(string whoTo, string attachment)
        {
            var mail = new MailMessage(
                new MailAddress("test@codcat.com", "Currency Graph"),
                new MailAddress(whoTo)
            )
            {
                Subject = "Currency Graph Update",
                Body = "View the update in attachment",
            };
            mail.Attachments.Add(new Attachment(attachment));

            new SmtpClient("smtp-190825.m25.wedos.net")
            {
                EnableSsl = true,
                Credentials = new NetworkCredential("test@codcat.com", "kI*2P]26Ay/1y"),
            }
            .Send(mail);
        }
    }
}
