﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.XPath;

namespace CurrencyGraphLib
{
    public class Downloader
    {

        /* Initialization */

        public Report GetTodaysReport()
        {
            var xml = new XmlDocument();
            xml.Load("https://www.cnb.cz/cs/financni_trhy/devizovy_trh/kurzy_devizoveho_trhu/denni_kurz.xml");
            var root = xml.DocumentElement;

            var csCZ = new CultureInfo("cs-CZ");
            var attrDate = root.SelectSingleNode("/kurzy/@datum");
            var date = DateTime.ParseExact(attrDate.Value.ToString(), "d.M.yyyy", csCZ);

            var lines = root.SelectNodes("/kurzy/tabulka/radek[@mnozstvi=1 and contains('GBP|USD|EUR|PLN|CNY|HKD', @kod)]");
            var report = new Report() { Date = date };
            foreach (XmlElement line in lines)
            {
                var code = line.GetAttribute("kod");
                var rate = line.GetAttribute("kurz");
                var currency = new Currency()
                {
                    Code = code,
                    Rate = Convert.ToDecimal(rate),
                };
                report.Currencies.Add(currency);
            }

            return report;
        }
        
    }
}
