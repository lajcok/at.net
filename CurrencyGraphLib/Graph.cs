﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurrencyGraphLib
{
    public class Graph
    {
        /* Properties */

        public readonly List<Report> Reports;

        public readonly string Output;

        /* Initialization */

        public Graph(History history, string output = "currency.bmp")
        {
            Reports = new List<Report>(history.GetReports(MAX_DATES));
            Reports.Reverse();
            Output = output;
        }

        /* Graph */

        private static readonly int IMG_WIDTH = 800;
        private static readonly int IMG_HEIGHT = 600;

        private static readonly int PAD_SM = 30;
        private static readonly int PAD_BG = 60;

        private static readonly int RATE_MAX = 35;
        private static readonly int RATE_STEP = 5;

        private static readonly int MAX_DATES = 10;

        private static readonly Color[] COLORS = {
            Color.Tomato, Color.Plum, Color.SeaGreen, Color.Purple, Color.SlateBlue, Color.Turquoise
        };

        private Graphics graphics;

        public void Draw()
        {
            using(var bitmap = new Bitmap(IMG_WIDTH, IMG_HEIGHT))
            {
                using (graphics = Graphics.FromImage(bitmap))
                {
                    DrawBackground();
                    DrawRateAxe();
                    DrawDateAxe();
                    DrawGraph();
                }
                bitmap.Save(Output);
            }
        }

        private void DrawBackground()
        {
            graphics.FillRectangle(
                new SolidBrush(Color.White),
                0, 0,
                IMG_WIDTH, IMG_HEIGHT
            );
            
        }

        private void DrawRateAxe()
        {
            var clr = Color.LightSlateGray;
            var pen = new Pen(clr, 2);

            // Axe + Grid
            int len = IMG_HEIGHT - PAD_SM - PAD_BG;
            int n_pt = RATE_MAX / RATE_STEP;
            int dif = len / n_pt;

            int pt_x = IMG_WIDTH - PAD_BG;
            int pt_y_base = IMG_HEIGHT - PAD_BG;

            // Axe
            graphics.DrawLine(
                pen,
                pt_x, PAD_SM,
                pt_x, pt_y_base
            );

            for(int i = 0; i <= n_pt; i++)
            {
                int pt_y = pt_y_base - dif * i;

                // Grid
                graphics.DrawLine(
                    new Pen(Color.LightGray, 1) {
                        DashPattern = new float[] { 5, 3 }
                    },
                    PAD_SM, pt_y,
                    IMG_WIDTH - PAD_BG, pt_y
                );

                // Values
                string rate_val = string.Format("{0} CZK", RATE_STEP * i);
                graphics.DrawString(
                    rate_val,
                    new Font(FontFamily.GenericSansSerif, 10),
                    new SolidBrush(clr),
                    pt_x + 4, pt_y
                );
            }
        }

        private void DrawDateAxe()
        {
            var clr = Color.LightSlateGray;
            var pen = new Pen(clr, 2);

            // Axe + Grid
            int len = IMG_WIDTH - PAD_SM - PAD_BG;
            int dif = len / MAX_DATES;

            int pt_y = IMG_HEIGHT - PAD_BG;
            int pt_x_base = IMG_WIDTH - PAD_BG;

            // Axe
            graphics.DrawLine(
                pen,
                PAD_SM, pt_y,
                pt_x_base, pt_y
            );
        
            int pt_x = pt_x_base;
            foreach (var report in Reports)
            {
                pt_x -= dif;

                // Grid
                graphics.DrawLine(
                    new Pen(Color.LightGray, 1)
                    {
                        DashPattern = new float[] { 5, 3 }
                    },
                    pt_x, pt_y,
                    pt_x, PAD_SM
                );
                
                // Values
                graphics.DrawString(
                    report.Date.ToString("dd.MM."),
                    new Font(FontFamily.GenericSansSerif, 10),
                    new SolidBrush(clr),
                    pt_x, pt_y + 4
                );
            }
        }

        private void DrawGraph()
        {
            // Location
            int len = IMG_WIDTH - PAD_SM - PAD_BG;
            int dif = len / MAX_DATES;
            int pt_x_base = IMG_WIDTH - PAD_BG;

            // Rate
            int height = IMG_HEIGHT - PAD_BG - PAD_SM;
            int rate_dif = height / RATE_MAX;

            int pt_x = pt_x_base;
            var latest = new Dictionary<string, Tuple<Point, Color>>();

            // Currency Legend
            var leg_pt = new Point(PAD_SM, IMG_HEIGHT - PAD_SM);

            var clr_it = COLORS.AsEnumerable().GetEnumerator();
            foreach (var report in Reports)
            {
                pt_x -= dif;

                // Currencies
                foreach(var currency in report.Currencies)
                {
                    int pt_y = IMG_HEIGHT - PAD_BG - (int)(rate_dif * currency.Rate);
                    var current = new Point(pt_x, pt_y);

                    if (latest.ContainsKey(currency.Code))
                    {
                        var last_pt = latest[currency.Code].Item1;
                        var pen_clr = latest[currency.Code].Item2;
                        var pen = new Pen(pen_clr, 2);

                        graphics.DrawLine(
                            pen,
                            last_pt,
                            current
                        );

                        latest[currency.Code] = new Tuple<Point, Color>(current, pen_clr);
                    }
                    else
                    {
                        var new_clr = clr_it.MoveNext() ? clr_it.Current : Color.Black;
                        latest.Add(currency.Code, new Tuple<Point, Color>(current, new_clr));

                        graphics.DrawString(
                            currency.Code,
                            new Font(FontFamily.GenericSansSerif, 10, FontStyle.Bold),
                            new SolidBrush(new_clr),
                            leg_pt
                        );

                        leg_pt.X += 40;
                    }
                }
            }
        }
    }
}
