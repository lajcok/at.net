﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

/*
// Visual C++ > Windows Desktop > Dynamic-Link Library Project
extern "C" __declspec(dllexport)
int sum(int values[], int length) {
    int sum = 0;
    for (int i = 0; i < length; i++) {
        sum += values[i];
    }
    return sum;
}
// Rebuild
// Copy DLL file to C#'s bin
*/

namespace Cv09_DLL
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] numbers = [1, 2, 3];
            System.Console.WriteLine("Sum: {0}", NativeCls.sum(numbers, numbers.Length));
        }
    }

    class NativeCls
    {
        [DllImport("MyDll.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int sum(int[] numbers, int length);
    }
}
