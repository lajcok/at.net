﻿using CurrencyGraphLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace CurrencyGraph
{
    public partial class CurrencyGraphService : ServiceBase
    {
        /* Initialization */

        public CurrencyGraphService()
        {
            InitializeComponent();
        }

        /* Properties */

        private Timer timer;

        /* Service Control */

        protected override void OnStart(string[] args)
        {
            timer = new Timer(5 * 60 * 1000) { AutoReset = true };
            timer.Elapsed += (object sender, ElapsedEventArgs e) => { DoReport(); };

            DoReport();
            timer.Start();
        }

        protected override void OnStop()
        {
            timer.Stop();
            timer.Dispose();
        }

        /* Reporting */

        private static void DoReport()
        {
            // Report download
            var downloader = new Downloader();
            var report = downloader.GetTodaysReport();

            // History log
            var history = new History();
            history.AddReport(report);

            // Graph
            var graph = new Graph(history);
            graph.Draw();

            // Email
            var email = new Email();
            email.Send("lajcok.jiri@gmail.com", graph.Output);
        }
    }
}
